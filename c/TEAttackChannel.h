/*

* Author:       Rick Candell (rick.candell@nist.gov)
*
* Organization: National Institute of Standards and Technology
*               U.S. Department of Commerce
* License:      Public Domain

*/

// TEErrorFreeChannel is used to sync simulation time with wall clock time

#ifndef __TEEATTACKCHANNEL_H__
#define __TEEATTACKCHANNEL_H__

#include <boost/random.hpp>
#include <iostream>
#include <ostream>
#include <string>

#include "TETypes.h"
#include "TEChannel.h"



enum DataType {
	UNDEFINED = 0,
    XMEAS = 1,
    XMV   = 2
}; 


enum AttackType {
	CONSTANT = 0,
	SLOPE,
	OSCILLATE
}; 

enum AttackSetType {
	SET = 0,
	ADD
};

struct Attack {
    double  	start;
    double  	end;
    double  	value;
	double		frequency;
	double 		phase;
    int     	target;
    DataType 	targetType;
	AttackType	attackType;
	AttackSetType attackSetType;

	Attack();

	friend std::ostream &operator<<(std::ostream &os, const Attack &a);
}; 



class TEAttackChannel : public TEChannel
{
	std::vector<Attack> m_attacks;
	int 				m_dataType;
	const double 	   *m_time;

public:
	// Construct a channel object with global packet error rate, 
	// length of the data, and initial values.
	TEAttackChannel(unsigned dlen, const std::string& attack_descriptor_path, const double *time, const double* init_values);

	// destroys all memory associate with object and deletes object
	virtual ~TEAttackChannel();

	// This method will apply the random channel to the input data
	// and return the data with channel applied.  If packets are dropped,
	// the last know value is returned.
	double* operator+(double* data);

	// overloaded output stream for channel
	friend std::ostream& operator<< (std::ostream&, const TEAttackChannel&);

private:
	TEAttackChannel();
	TEAttackChannel(const TEAttackChannel&);
	TEAttackChannel& operator=(const TEAttackChannel&) { return *this; };

};

#endif 
