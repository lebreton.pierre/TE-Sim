/*

* Author:       Rick Candell (rick.candell@nist.gov)

* Organization: National Institute of Standards and Technology
*               U.S. Department of Commerce
* License:      Public Domain

*/

#include <iostream>     // std::cout, std::ostream, std::ios
#include <fstream>
#include <utility>
#include <iomanip>
#include <string>
#include <vector>

#include <json_spirit.h>


#include "TEAttackChannel.h"
#include "TEPlant.h"

Attack::Attack() {
    start = 0.;
    end = 0.;
    value = 0.;
    target = 0;
    frequency = 0.;
    phase = 0.;
    targetType = UNDEFINED;
    attackType = CONSTANT;
    attackSetType = SET;
}

std::ostream &operator<<(std::ostream &os, const Attack &a) {
    return (os << "start: " << a.start << "\nend: " << a.end << "\nvalue: " << a.value << "\ntarget: " << a.target << "\nfrequency: " << a.frequency << "\nphase: " << a.phase << "\ntargetType: " << a.targetType << "\nattackType: " << a.attackType << "\nattackSetType: " << a.attackSetType );
}


TEAttackChannel::TEAttackChannel(unsigned dlen, const std::string& attack_descriptor_path, const double *time, const double* init_values) 
	: TEChannel(dlen, init_values), m_dataType(0), m_time(time)
{
    if(TEPlant::NY == dlen) {
        m_dataType = XMEAS;
    } else if(TEPlant::NU == dlen) {
        m_dataType = XMV;
    } else {
        m_dataType = UNDEFINED;
    }

    std::ifstream is(attack_descriptor_path.c_str());
    json_spirit::Value value;
    json_spirit::read( is, value );

    json_spirit::Object& categories = value.get_obj();
    for( json_spirit::Object::size_type j = 0; j != categories.size(); ++j ) {
        const json_spirit::Pair& pairCategory = categories[j];


        const std::string& categoryName                  = pairCategory.name_;
        const json_spirit::Value&  valueCategory = pairCategory.value_;

        if(categoryName == "attacks") {
            
        	const json_spirit::Array& entries = valueCategory.get_array();
	        for(unsigned int i = 0 ; i < entries.size() ; ++i) {
	            const json_spirit::Object& entry = entries[i].get_obj();

                Attack a;

	            for(json_spirit::Object::size_type k = 0 ; k != entry.size() ; ++k) {
	                const json_spirit::Pair& pair = entry[k];
	                const std::string& name  = pair.name_;
	                const json_spirit::Value& localV = pair.value_;

	                if(name == "start")
		                a.start = localV.get_real();

                    if(name == "end")
		                a.end = localV.get_real();

					if(name == "value")
						a.value = localV.get_real();

                    if(name == "target")
						a.target = localV.get_int();

                    if(name == "targetType") {
                        if(localV.get_str() == "xmeas")
                            a.targetType = XMEAS;
                        else if(localV.get_str() == "xmv")
                            a.targetType = XMV;
                        else
                            a.targetType = UNDEFINED;
                    }

                    if(name == "attackType") {
                        if(localV.get_str() == "constant")
                            a.attackType = CONSTANT;

                        if(localV.get_str() == "slope")
                            a.attackType = SLOPE;

                        if(localV.get_str() == "oscillate")
                            a.attackType = OSCILLATE;
                    }

                    if(name == "setType") {
                        std::cout << "set set type";
                        if(localV.get_str() == "set")
                            a.attackSetType = SET;

                        if(localV.get_str() == "add")
                            a.attackSetType = ADD;
                    }

                    if(name == "frequency") {
                        a.frequency = localV.get_real();
                    }

                    if(name == "phase") {
                        a.phase = localV.get_real();
                    }
	            }

                if(a.targetType == m_dataType) {
                    m_attacks.push_back(a);
                }                
	        }
        } 
    }
}

TEAttackChannel::~TEAttackChannel()
{
}

double * TEAttackChannel::operator+(double* data)
{

	for (unsigned ii = 0; ii < m_dlen; ii++)
	{
		m_data[ii] = data[ii];
	}

    for(size_t i = 0 ; i < m_attacks.size() ; ++i) {
        if(*m_time >= m_attacks[i].start && *m_time <= m_attacks[i].end) {

            double value = 0;
            switch(m_attacks[i].attackType) {
                case CONSTANT:
                    value = m_attacks[i].value;
                    break;

                case OSCILLATE:
                    value = m_attacks[i].value * std::cos((*m_time - m_attacks[i].start)*m_attacks[i].frequency + m_attacks[i].phase);
                    break;

                case SLOPE:
                    value = m_attacks[i].value * (*m_time - m_attacks[i].start) / (m_attacks[i].end - m_attacks[i].start);
                    break;
            }

            switch(m_attacks[i].attackSetType) {
                case SET:
                    m_data[m_attacks[i].target] = value;
                    break;

                case ADD:
                    //std::cout << "add " << *m_time << " " << value << " ";
                    m_data[m_attacks[i].target] += value;
                    break;
            }
        }
    }
	
	return m_data;
}


